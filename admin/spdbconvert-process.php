<?php

require_once("../../../../wp-config.php");
global $wpdb;

$t = $_GET['t'];

if ($t == 'Finished') {
	echo "<br /><br /><p>Conversion Finished<br />";
	die();
}

$sql = "SHOW TABLE STATUS WHERE NAME = '$t'";
$table = $wpdb->get_row($sql);

if($table) {
	if($table->Engine != 'InnoDB') {
		$sql = "ALTER TABLE $t ENGINE=INNODB";
		if($wpdb->query($sql) === false) {
			$m = "<p class='spdbFailure'><b>$t</b>: Unable to convert to InnoDB</p>";
		} else {
			$m = "<p class='spdbSuccess'><b>$t</b>: Successfully convered to InnoDB</p>";
		}
	} else {
		$m = "<p class='spdbSuccess'><b>$t</b>: Already using InnoDB</p>";

	}
} else {
	$m = "<p class'spdbFailure'>Not found in the database</p>";
}

echo $m;

die();

?>