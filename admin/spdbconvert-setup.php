<?php

?>
	<div class="wrap nosubsub">
	<div id="icon-plugins" class="icon32"><br /></div>
	<h2>Simple:Press Database Table Converter</h2>
	<div style="clear: both"></div>
	<div id="spdbContainer">
	<div id="spdbMainHead">
	<h1>Convert Database Tables to the InnoDB Engine</h1>
	<div style="clear: both"></div>
	</div><br />
<?php

	if(empty($_POST)) {
		sp_sbconvert_select();
	} else {
		sp_dbconvert_form();
	}
?>
	</div></div>
<?php

function sp_sbconvert_select() {
?>
	<form action="<?php echo(site_url()); ?>/wp-admin/admin.php?page=sp-db-converter%2Fadmin%2Fspdbconvert-setup.php" method="post" id="spSelect" name="spSelect">
	<input type="submit" class="button-primary" id="spOnly" name="spOnly" value="Simple:Press Tables Only"/>
	<input type="submit" class="button-primary" id="spAll" name="spAll" value="Simple:Press and All Other Tables"/>
	</form>
<?php
}

function sp_dbconvert_form() {
	global $wpdb;

	$like = '';
	$scope = '';
	if(isset($_POST['spOnly'])) {
		$like = "Name LIKE '".$wpdb->prefix."sf%' AND";
		$scope = 'Simple:Press';
	}

	$sql = "SHOW TABLE STATUS WHERE $like ENGINE = 'MyISAM'";
	$tables = $wpdb->get_col($sql);

	if($tables) {
		$ripe = count($tables);
?>
		<div id="spdbForm" class="spdbMainPanel">
		<p><b>This process will convert your <?php echo($scope); ?> database MyISAM tables 
		to use the more efficient MySQL InnoDB storage engine.<b><br /><br /></p>
		<h3>IMPORTANT</h3>
		<p><b>It is strongly recommended that you make a full <u>database backup</u> prior to 
		running this conversion. Remember - you may need to restore it!</b></p><br />
		<p><b>For safety and to ensure no data corruption you should place your site 
		in <u>Maintenance Mode</u>. There is a simple and effective <a href="http://sw-guide.de/wordpress/plugins/maintenance-mode/">WordPress plugin</a> to enable this.</b></p><br />
<?php
		$phpfile = SPDB_URL."admin/spdbconvert-process.php";
		$tables = implode(',', $tables).',Finished';
		$gif = SFCOMMONIMAGES.'working.gif';

	} else {
?>
		<div id="spdbForm" class="spdbMainPanel">
		<p>There are no <?php echo($scope); ?> tables found requiring conversion</op>
		</div>
<?php
		return;
	}
?>
	<h4>Tables to be converted: <?php echo($ripe); ?></h4>
	<input type="button" class="button-primary" id="doit" name="doit" value="Convert Now" onclick="spjPerformConvert('<?php echo($phpfile);?>', '<?php echo($tables);?>', '<?php echo($gif);?>');";/>
	</div>
<?php
}

?>