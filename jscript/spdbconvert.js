function spjPerformConvert(phpUrl, tables, gif) {
	var url = '';
	var t = tables.split(",");

	var html = '<img src="' + gif + '" /><h4>Please be patient - depending on the size of your database tables this may take a little while...</h4>';
	jQuery("#spdbForm").html(html);

	for (i = 0; i < t.length; i++) {	
		url=phpUrl+'?t='+t[i];
		jQuery('#spdbForm').load(url, function(a, b) {
			if (b == 'success') {
				html += a;
				jQuery("#spdbForm").html(html);
			} else {
				html += 'AJAX Error';
				jQuery("#spdbForm").html(html);
			}	
		});	
	}
}
