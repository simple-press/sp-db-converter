<?php
/*
Plugin Name: Simple:Press DB Converter
Plugin URI: http://simple-press.com
Description: Convert Simple:Press and optionally WordPress database tables to use the InnoDB Engine
Version: 2.0
Author: Andy Staines & Steve Klasen
Author URI: http://simple-press.com
WordPress Version 4.0 and above
Simple-Press Version 5.5.4 and above
*/

/*  Copyright 2017  Andy Staines & Steve Klasen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    For a copy of the GNU General Public License, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

# ------------------------------------------------------------------
# Define Constants
# ------------------------------------------------------------------
	define('SPDB_URL',			WP_PLUGIN_URL.'/sp-db-converter/');	# Plugin folder url
	define('SPDB_DIR',			WP_PLUGIN_DIR.'/sp-db-converter/');	# Plugin path

# ------------------------------------------------------------------
# Action/Filter Hooks
# Create new menu item in the WP Simple:Press menu
# ------------------------------------------------------------------
add_action('admin_menu', 				'spdb_menu');

# ------------------------------------------------------------------------------------------
# Determine quickly if admin and then if importer admin page load

if(is_admin() && isset($_GET['page']) && stristr($_GET['page'], 'sp-db-converter/')) {
	add_action('admin_print_styles', 	'spdb_css');
	add_action('admin_enqueue_scripts', 'spdb_scripts');
}

# ------------------------------------------------------------------
# Add the importer to SP menu item into th SP menu
# ------------------------------------------------------------------
function spdb_menu() {
	$url = 'sp-db-converter/admin/spdbconvert-setup.php';
	add_submenu_page('simple-press/admin/panel-forums/spa-forums.php', esc_attr('InnoDB Converter'), esc_attr('InnoDB Converter'), 'read', $url);
}

# ------------------------------------------------------------------
# Loads up the forum admin CSS
# ------------------------------------------------------------------
function spdb_css() {
	wp_register_style('spDBStyle', SPDB_URL.'css/spdbconvert.css');
	wp_enqueue_style( 'spDBStyle');
}

# ------------------------------------------------------------------
# Load up the javascript we need
# ------------------------------------------------------------------
function spdb_scripts() {
	wp_register_script('jquery', WPINC.'/js/jquery/jquery.js', false, false, false);
	wp_enqueue_script('jquery');
	wp_enqueue_script('spdb', SPDB_URL.'jscript/spdbconvert.js', array('jquery'), false);
}

?>